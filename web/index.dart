import 'package:angular2/platform/browser.dart';
import 'package:angular2/angular2.dart';
import 'package:semplex_generic_ui/Elements/merge_panels/merge_panels.dart';
import 'package:semplex_generic_ui/Elements/popup_dialog/popup_dialog.dart';
import 'package:semplex_generic_ui/Elements/login/login.dart';
import 'package:leafletjs/leafletjs.dart';

import 'package:cork/src/binding/runtime.dart' as DI show Binding, Provider;
import 'package:SemplexClientCmn/Utils/DI/Net.dart' as DI;
import 'package:SemplexClientCmn/Utils/DI/HtmlStorage.dart' as DI;
import 'package:SemplexInvModels/Controllers/AppMan.dart';

import 'package:SemplexInvModels/Models/User.dart';

import 'dart:async';

@Component(
  selector: 'start-point',
  templateUrl: 'start-point.html',
  directives: const [MergePanels, Leafletjs, Login, PopupDialog]
)
class StartPoint implements OnInit
{
  AppMan _man = new AppMan();

  static bool initAppMan() {
    List<DI.Binding> modules = [];
    modules.addAll(DI.bindingsForNetModuleEntrypoint);
    modules.addAll(DI.bindingsForHtmlStorageEntrypoint);

    AppMan.urlBase = "dal.semplex.ru";

    AppMan.Init(new DI.Injector(modules));
    return true;
  }

  @override
  ngOnInit() {}

  Function login = UserUtils.LoginUser;
}

void main() {
  StartPoint.initAppMan();
  bootstrap(StartPoint);
  enableProdMode();
}
