library semplex.generic.ui.pipes.str2currency;
import 'package:angular2/angular2.dart';

@Injectable()
@Pipe(name: 'asCurrency')
class Str2Currency implements PipeTransform {
  transform(var value) {
    return forward(value);
  }

  String forward(num v) {
    String res = "";
    if(v.abs() > v.abs().truncate()) {
      res = "$v";
    } else {
      res = "${v.truncate()}";
    }

    List<String> parts = new List();

    int start = res.length;

    int counter = 1;

    for(int ind = start - 1; ind >= 0; --ind) {
      if(counter == 3) {
        parts.add(res.substring(ind, ind + 3));
        counter = 1;
      } else {
        ++counter;
      }
    }

    if (res.length % 3 != 0) {
      parts.add(res.substring(0, res.length - (parts.length * 3)));
    }

    res = "";
    start = 0;
    for(String part in parts.reversed) {
      if(start!=0) res += " $part";
      else res += part;
      ++start;
    }

    return res;
  }
}
