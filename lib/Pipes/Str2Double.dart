library semplex.generic.ui.pipes.str2double;
import 'package:angular2/angular2.dart';

@Injectable()
@Pipe(name: 'asDouble')
class Str2Double implements PipeTransform {
  transform(String value) {
    return double.parse(value);
  }
}
