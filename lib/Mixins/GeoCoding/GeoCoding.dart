library semplex.generic.ui.mixins.geo.code.reverse;
import 'dart:async';
import 'package:SemplexClientCmn/Utils/Interfaces/IRestAdapter.dart';
import 'package:SemplexInvModels/Models/Estate.dart';

abstract class GeoCodeReverse
{

  IRestAdapter get Net;

  SetShortAddress(String addr);

  parseGeoCode(Map data) {
    if(data.containsKey('address')) {
      Map address = data['address'];
      final String house_number = address['house_number'];
      final String road = address['road']??address['construction'];

      SetShortAddress("${road??''} ${house_number??''}");
    }
  }

  geoCode(Estate obj)
  {
    final String baseUrl =
     "http://nominatim.openstreetmap.org/reverse/?format=json&addressdetails=1"
     "&accept-language=ru";
    double lat = obj.Geo['coordinates'][0];
    double lon = obj.Geo['coordinates'][1];
    final String objUrl = "$baseUrl&lat=${lon}&lon=${lat}";

    return Net.Get(objUrl, credentials: false).then(parseGeoCode);
  }

}

abstract class GeoCodeForward
{
  IRestAdapter get Net;

  Future<List<Map>> query(String query) {
    final String queryUrl =
     "http://nominatim.openstreetmap.org/search?format=json&addressdetails=1"
     "&accept-language=ru"
     '&q="${query}"';
     return Net.Get(queryUrl, credentials: false);
  }
}
