library semplex.generic.ui.elements.merge_panels;
import 'dart:html';
import 'dart:async';

import 'package:angular2/angular2.dart';

import 'package:ng_mediaquery/ng_mediaquery.dart';

@Component(
  selector: 'merge-panels',
  templateUrl: 'merge_panels.html',
  styleUrls: const['merge_panels.css'],
  directives: const [MediaQuery, NgStyle, NgClass]
)
class MergePanels implements OnInit {
  bool _phoneScreen = false;

  @Output() EventEmitter<bool> active = new EventEmitter();
  @Output() EventEmitter<bool> changeStart = new EventEmitter();

  @Input() String duration = '0.3';
  @Input('left-width') String left_width = '60%';
  @Input('right-width') String right_width = '40%';
  @Input('z-index-max') String maxZIndex = '1001';
  @Input('z-index-min') String minZIndex = '0';
  @Input() String horizontal;

  bool isActive = false;
  String zIndex;

  DivElement _moreContainer;
  DivElement _leftMenu;
  DivElement _rightMenu;

  ElementRef _host;

  MergePanels(this._host);

  MutationObserver observer;

  Element $(String selector) => (_host.nativeElement as Element)
      .querySelector(selector);

  Element get host => (_host.nativeElement as Element);

  setHostZindex() {
    host.style.zIndex = zIndex;
  }

  @override
  ngOnInit() {
    zIndex = minZIndex;
    observer = new MutationObserver(_listenChanges);
    observer.observe(host, attributes: true);
    _moreContainer = $('#more-container');
    _leftMenu = $('#more-info-left');
    _rightMenu = $('#more-info-right');
    toggleEvents(false);
    host.setAttribute('off', '');
  }

  _listenChanges(List<MutationRecord> mutations, MutationObserver observer)
  {
    mutations.forEach((MutationRecord rec){
      if(rec.type == "attributes"){
        if(host.attributes.containsKey(rec.attributeName)) {
          if(rec.attributeName == 'on')  internalTogglePanels();
        } else {
          if(rec.attributeName == 'on')  internalTogglePanels();
        }
      }
    });
  }

  _toggleHostAttribute() {
    if(host.attributes.containsKey('off'))
    {
      host.attributes.remove('off');
      host.setAttribute('on', '');
    } else {
      host.attributes.remove('on');
      host.setAttribute('off', '');
    }
  }

  Future _toggleLeft() {
    Completer comp = new Completer();
    window.requestAnimationFrame((_) {
      if(_leftMenu.attributes.containsKey('off'))
      {
       _leftMenu.attributes.remove('off');
       _leftMenu.setAttribute('on', '');
      } else {
        _leftMenu.attributes.remove('on');
        _leftMenu.setAttribute('off', '');
      }
      comp.complete();
    });
    return comp.future;
  }

  Future _toggleRight() {
    Completer comp = new Completer();
    window.requestAnimationFrame((_) {
      if(_rightMenu.attributes.containsKey('off'))
      {
        _rightMenu.attributes.remove('off');
        _rightMenu.setAttribute('on', '');
      } else {
        _rightMenu.attributes.remove('on');
        _rightMenu.setAttribute('off', '');
      }
      comp.complete();
    });
    return comp.future;
  }

  Future _toggleContainer() {
    Completer comp = new Completer();
    window.requestAnimationFrame((_) {
      if(_moreContainer.attributes.containsKey('off')) {
        _moreContainer.attributes.remove('off');
        _moreContainer.setAttribute('on', '');
      } else {
        _moreContainer.attributes.remove('on');
        _moreContainer.setAttribute('off', '');
      }
      comp.complete();
    });
    return comp.future;
  }

  set phoneScreen(bool val) {
    _phoneScreen = val;
    _phoneScreen ? host.setAttribute('phone-screen', '')
                 : host.attributes.remove('phone-screen');
  }
  bool get phoneScreen => _phoneScreen || horizontal != null;

  String get transition => 'left ${duration}s, right ${duration}s';
  String get leftTop => '0px';
  String get leftWidth => phoneScreen ? '100%' : '${left_width}';
  String get leftHeight => phoneScreen ? '${left_width}' : '100%';
  String get rightTop => phoneScreen ? '${left_width}' : '0px';
  String get rightWidth => phoneScreen ? '100%' : '${right_width}';
  String get rightHeight => phoneScreen ? '${right_width}': '100%';

  toggleEvents(bool toggle) {
    if(toggle) {
      host.style.pointerEvents = 'auto';
    } else {
      host.style.pointerEvents = 'none';
    }
  }

  Future toggleLeft() => _toggleLeft();

  Future toggleRight() => _toggleRight();

  Future internalTogglePanels() async {
    setZindex() async
    {
      zIndex = zIndex == minZIndex ? maxZIndex : minZIndex;
      setHostZindex();
    }
    List futures = []..add(toggleLeft())..add(toggleRight());

    if(zIndex == maxZIndex) {
      int mill = 0;
      try {
         mill = (double.parse(duration) * 1000).ceil();
      } catch(e){}
      new Future.delayed(new Duration(milliseconds: mill), setZindex);
    } else {
      setZindex();
    }

    changeStart.emit(isActive);

    return Future.wait(futures).then((_){
      isActive = !isActive;
      toggleEvents(isActive);
      active.emit(isActive);
    });
  }

  togglePanels() => _toggleHostAttribute();

  onMoreToggle() => togglePanels();
  onMoreLeftToggle() => toggleLeft();
  onMoreRightToggle() => toggleRight();
}
