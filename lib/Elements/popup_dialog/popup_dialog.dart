library semplex.generic.ui.elements.popup_dialog;
import 'dart:html';
import 'dart:async';

import 'package:angular2/angular2.dart';

@Component(
  selector: 'popup-dialog',
  styleUrls: const ['popup_dialog.css'],
  templateUrl: 'popup_dialog.html'
)
class PopupDialog implements OnInit {
  ElementRef _host;
  MutationObserver observer;

  Element $(String selector) => (_host.nativeElement as Element)
      .querySelector(selector);

  Element get host => (_host.nativeElement as Element);

  PopupDialog(this._host);

  _listenChanges(List<MutationRecord> mutations, MutationObserver observer)
  {
    mutations.forEach((MutationRecord rec){
      if(rec.type == "attributes"){
        if(host.attributes.containsKey(rec.attributeName)) {
          if(rec.attributeName == 'on')  Show();
        } else {
          if(rec.attributeName == 'on')  Hide();
        }
      }
    });
  }

  @override
  ngOnInit() {
    observer = new MutationObserver(_listenChanges);
    observer.observe(host, attributes: true);
  }

  Show() {
    host.style.display = "flex";
  }

  Hide() {
    host.style.display = "none";
  }
}
