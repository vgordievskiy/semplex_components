library semplex.generic.ui.elements.login;
import 'dart:html';
import 'dart:async';

import 'package:angular2/angular2.dart';
import 'package:angular2_rbi/directives.dart';

typedef Future<String> TLoginFunc(String login, String password);

@Component(
  selector: 'login',
  templateUrl: 'login.html',
  styleUrls: const['login.css'],
  directives: const [CORE_DIRECTIVES, FORM_DIRECTIVES,
                     MaterialTextfield, MaterialButton],
  encapsulation: ViewEncapsulation.None
)
class Login implements OnInit {
  ElementRef _host;

  Login(this._host);

  @Input() TLoginFunc login;
  @Output() EventEmitter loginStatus = new EventEmitter();

  String user;
  String passwd;

  MutationObserver observer;

  Element $(String selector) => (_host.nativeElement as Element)
      .querySelector(selector);

  Element get host => (_host.nativeElement as Element);

  @override
  ngOnInit() {}

  doLogin() {
    login( user , passwd )
      .then((String url) {
        loginStatus.add({ 'status' : true, 'url' : url });
    })
      .catchError((err){
        loginStatus.add({ 'status' : false, 'url' : null });
    });
  }

}
