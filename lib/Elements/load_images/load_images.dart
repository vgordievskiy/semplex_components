library semplex.generic.ui.elements.load_images;
import 'dart:html';
import 'dart:async';

import 'package:angular2/angular2.dart';

@Component(
  selector: 'load-images',
  templateUrl: 'load_images.html',
  styleUrls: const['load_images.css'],
  directives: const [CORE_DIRECTIVES]
)
class LoadImages implements OnInit {

  ElementRef _host;

  LoadImages(this._host);
  InputElement _fileInput;
  DivElement _dropZone;
  DivElement _listFiles;

  List<File> Files = <File>[];

  @Input('image-width') String imageWidth;
  @Input('image-height') String imageHeight;

  @Output() EventEmitter<File> filesChange = new EventEmitter();

  Element $(String selector) => (_host.nativeElement as Element)
      .querySelector(selector);

  Element get host => (_host.nativeElement as Element);

  @override
  ngOnInit()
  {
    _fileInput = $('#files');
    _fileInput.onChange.listen((e) => _onFileInputChange());
    _dropZone = $('#drop-zone');
    _listFiles = $('#list-files');
    _initDnD();
  }

  _initDnD() async {
    _dropZone.onDragOver.listen(_onDragOver);
    _dropZone.onDragEnter.listen((e) => _dropZone.classes.add('hover'));
    _dropZone.onDragLeave.listen((e) => _dropZone.classes.remove('hover'));
    _dropZone.onDrop.listen(_onDrop);
  }

  void _onDragOver(MouseEvent event) {
    event.stopPropagation();
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';
  }

  void _onDrop(MouseEvent event) {
    event.stopPropagation();
    event.preventDefault();
    _dropZone.classes.remove('hover');
    _onFilesSelected(event.dataTransfer.files);
  }

  void _onFileInputChange() {
    _onFilesSelected(_fileInput.files);
  }

  void _onFilesSelected(List<File> files) {
    _listFiles.children.clear();

    Files.clear();
    Files.addAll(files);

    for (File file in files) {
      // If the file is an image, read and display its thumbnail.
      if (file.type.startsWith('image')) {
        DivElement thumbHolder = new Element.tag('div');
        thumbHolder.style.alignSelf = 'center';
        FileReader reader = new FileReader();
        reader.onLoad.listen((e) {
          ImageElement thumbnail = new ImageElement(src: reader.result);
          thumbnail.style.maxHeight = imageHeight ?? '';
          thumbnail.style.maxWidth = imageWidth ?? '';
          thumbnail.classes.add('thumb');
          thumbHolder.nodes.add(thumbnail);

          filesChange.emit(file);
        });
        reader.readAsDataUrl(file);
        _listFiles.append(thumbHolder);
      }
    }
  }

  clear() { Files.clear(); _listFiles.children.clear();}
}
