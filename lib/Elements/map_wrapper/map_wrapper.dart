library semplex.generic.ui.elements.map_wrapper;
import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'dart:js';

import 'package:angular2_rbi/directives.dart';

import 'package:angular2/angular2.dart';
import 'package:leafletjs/leafletjs.dart' as lmap;
import 'package:simple_features/simple_features.dart' as Geo;
import 'package:observe/observe.dart';

import 'package:SemplexInvModels/Models/Estate.dart';

final String iconUrl = 'Resources/icons/map-markers/marker.png';
final String iconRedUrl = 'Resources/icons/map-markers/marker_red.png';

typedef Future<String> TLoginFunc(String login, String password);

enum MarkerAction
{
  EDIT,
  DELETE
}

enum MarkerType
{
  NEW,
  EXIST
}

class MarkerEvent {
  final int id;
  final MarkerType type;
  final MarkerAction action;
  final dynamic geoJSON;
  final dynamic data;
  MarkerEvent(this.id, this.type, this.action,
              {this.geoJSON: null, this.data: null});
}

@Component(
  selector: 'geo-map',
  templateUrl: 'map_wrapper.html',
  styleUrls: const['map_wrapper.css'],
  encapsulation: ViewEncapsulation.None,
  directives: const[lmap.Leafletjs, MaterialButton]
)
class MapWrapper implements OnInit, AfterViewInit {
  ElementRef _host;
  NgZone _ngZone;
  @ViewChild(lmap.Leafletjs) lmap.Leafletjs map;

  MapWrapper(this._host, NgZone this._ngZone);

  @Input('map-type') String mapType = 'OSM-Night';

  @Input('is-editable') String isEditable = 'false';
  bool get IsEditable => JSON.decode(isEditable);

  @Input('is-empty') String isEmpty = 'false';
  bool get IsEmpty => JSON.decode(isEmpty);

  lmap.Icon defMarkerIcon;
  lmap.Icon redMarkerIcon;
  Map<int, int> Markers = new Map();
  Map<Type, Function> handlers;
  var lastRegion;
  int _autoLoad = 0;

  @Output('new-object') EventEmitter<MarkerEvent> newObj = new EventEmitter();
  @Output('object') EventEmitter<MarkerEvent> object = new EventEmitter();

  /*-----------Handlers bock----------------*/
  createBt(String type) {
    return new ButtonElement()
    ..classes.addAll(['mdl-button','mdl-js-button', 'mdl-button--icon'])
    ..appendHtml('<i class="material-icons">${type}</i>')
    ..style.alignSelf = 'center';
  }

  handlePropertyChanges(PropertyChangeRecord change) {
    if (_autoLoad != 0) return;
    if (change.name == #Region) {
      getAllBoundedObjects(change.newValue);
      lastRegion = change.newValue;
    }
  }

  handleMouseEvents(lmap.MouseEvent  evt) {
    if (IsEditable == false) return;
    if (evt.type == "dblclick") {

      String title = 'New Object';
      if(evt.data != null && (evt.data is Map)
         && (evt.data as Map).containsKey('title')) {
           title = evt.data['title'];
      }

      DivElement el = new Element.div();
      DivElement buttons = new DivElement();
      el.classes.addAll(['layout', 'vertical']);
      buttons.classes.addAll(['layout', 'horizontal']);
      el.appendHtml('<a>${title}</a>');
      ButtonElement btDel = createBt('delete');
      ButtonElement btShow = createBt('create');

      buttons.nodes.addAll([btDel, btShow]);
      el.nodes.add(buttons);

      lmap.Icon icon = redMarkerIcon;

      int id = map.AddMarker(evt.GeoPnt, title: title,
                             popup: el, icon : icon);

      btShow.onClick.listen((MouseEvent e){
        lmap.Marker marker = map.GetMarker(id);
        var geoJSON = new Map.from(lmap.toDart(marker.toGeoJSON()));
        newObj.emit(new MarkerEvent(id, MarkerType.NEW,
                                    MarkerAction.EDIT,
                                    geoJSON: geoJSON));
      });

      btDel.onClick.listen((MouseEvent evt){
        map.RemoveMarker(id);
      });
    }
  }

  handleMarkerEvents(lmap.MarkerEvent  evt) {
    int id = evt.Id;
  }

  _initListeners() async {
    map.changes.listen((List<ChangeRecord> changes){
      for(var change in changes) {
        if (handlers.containsKey(change.runtimeType))
          handlers[change.runtimeType](change);
      }
    });
  }

  initHadnlers() {
    handlers = {
     PropertyChangeRecord : handlePropertyChanges,
     lmap.MarkerEvent : handleMarkerEvents,
     lmap.MouseEvent : handleMouseEvents,
    };
  }
  /*-----------------------------------------*/


  MutationObserver observer;

  Element $(String selector) => (_host.nativeElement as Element)
      .querySelector(selector);

  Element get host => (_host.nativeElement as Element);

  @override
  ngOnInit() {
    _ngZone.runOutsideAngular(() {
        lmap.Leafletjs.initJsPart().then((_) {
        initHadnlers();
        _InitDefaultIconStyle();
        if(!IsEmpty) _initListeners();
      });
    });
  }

  @override
  ngAfterViewInit() {
    new Future.delayed(new Duration(milliseconds: 200), () {
      if (IsEditable == true) map.doubleClickZoom.disable();
      getAllBoundedObjects(map.Region);
    });
  }

  void _InitDefaultIconStyle() {
    /*Init marker style*/
    {
      ImageElement image =  new ImageElement(src: iconUrl);
      image.onLoad.listen((var el){
        defMarkerIcon = new lmap.Icon(new lmap.IconOptions(
          iconUrl : iconUrl,
          iconAnchor: [image.width ~/ 2, image.height],
          popupAnchor: [image.width ~/(-2), image.height*(-1)]
        ));
      });
    }

    /*Init red marker style*/
    {
      ImageElement image =  new ImageElement(src: iconUrl);
      image.onLoad.listen((var el){
        redMarkerIcon = new lmap.Icon(new lmap.IconOptions(
          iconUrl : iconRedUrl,
          iconAnchor: [image.width ~/ 2, image.height],
          popupAnchor: [image.width ~/(-2), image.height*(-1)]
        ));
      });
    }
  }

  addPoint(Estate obj, {bool highLight: false }) {
    var geo = obj.Geo;
    var geom = Geo.parseGeoJson(JSON.encode(geo));
    lmap.LatLng pnt = new lmap.LatLng(geom.y, geom.x);
    {
      DivElement el = new Element.div();
      el.classes.addAll(['layout', 'vertical']);
      el.appendText('${obj.Name}');

      lmap.Icon icon = highLight == false ? defMarkerIcon : redMarkerIcon;
      int id = map.AddMarker(pnt, title: obj.Name, popup: el, icon : icon);
      Markers[obj.Id] = id;

      if(IsEditable) {
        DivElement buttons = new DivElement()
          ..classes.addAll(['layout', 'horizontal', 'center']);
        ButtonElement btShow = createBt('create');

        buttons.nodes.addAll([btShow]);

        btShow.onClick.listen((MouseEvent e){
          object.emit(new MarkerEvent(id, MarkerType.EXIST,
                                      MarkerAction.EDIT,
                                      data: obj));
        });

        el.nodes.add(buttons);
      }
    }
  }

  mergeObjects(List<Estate> objects) {
    Set<int> cachedNewCommer = new Set();
    for(Estate obj in objects) {
      cachedNewCommer.add(obj.Id);
      if (!Markers.containsKey(obj.Id)) {
        addPoint(obj);
      }
    }
    Map<int, int> markers = new Map.from(Markers);
    for(int id in markers.keys) {
      if (!cachedNewCommer.contains(id)) {
        map.RemoveMarker(Markers[id]);
        Markers.remove(id);
      }
    }
  }

  getAllBoundedObjects(lmap.LatLngBounds region) async {
    List<Estate> objects = await EstateUtils.getAllBoundedObjects(region);
    return new Future(() => mergeObjects(objects));
  }

  Future onInvalidateSize()
  {
     return map.Invalidatesize();
  }

  onMapAdd(var detail)
  {
     List<dynamic> obj = [ detail ];
     mergeObjects(obj);
  }

  onMapClear()
  {
    Markers.clear();
    map.ClearAllMarkers();
  }

  Future onMapChangeZoom(var detail)
  {
    return map.SetZoom(detail);
  }

  Future onMapChangeView(var detail)
  {
    var geomPnt = Geo.parseGeoJson(JSON.encode(detail));
    lmap.LatLng pnt = new lmap.LatLng(geomPnt.y, geomPnt.x);
    return map.SetCenter(pnt);
  }

  onMapUpdate(var detail) {
    getAllBoundedObjects(lastRegion);
  }

  Future highLight(Estate obj) async {
    addPoint(obj, highLight: true);
    lmap.Marker marker = map.GetMarker(Markers[obj.Id]);
    return new Future(() => marker.openPopup());
  }

  Future onHighlight(Map<String, dynamic> detail) async {
    Estate obj = detail['object'];
    if (detail.containsKey('zoom')) {
      Map<String, dynamic> zoom = detail['zoom'];
      int scale = zoom['scale'];
      onMapAutoload(false);
      await onMapChangeZoom(scale);
      await onMapChangeView(obj.Geo);
      await highLight(obj);
      onMapAutoload(true);
      return;
    }
    highLight(obj);
  }

  onMapAutoload(var detail) {
    if (detail == true) {
      --_autoLoad;
      if (_autoLoad < 0) _autoLoad = 0;
    }
    else {
      ++_autoLoad;
    }
  }
}
